<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StudentLoginModel;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;


class StudentController extends Controller
{
    public function LoginActionHandler(Request $request) {
        $credentials = $request->only(['email', 'password']);
        $studentAuth = auth()->guard('student');
        if($studentAuth->attempt($credentials)){
            return back();
        }else{
            return redirect()->back()->withErrors('Invalid Login');
        }
    }

    public function LoginViewHandler()
    {
        // "login_student_59ba36addc2b2f9401580f014c7f58ea4e30989d" => 1
        dd(Session::all());
        return view('loginview');
    }

    public function LoginStatusHandler()
    {
       dd( StudentLoginModel::getLoggedStudent());
    }

    public function LogoutHandler()
    {

    }
}
