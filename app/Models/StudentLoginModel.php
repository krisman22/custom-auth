<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\User as Authenticatable;

class StudentLoginModel extends Authenticatable
{
    protected $table = 'student';
    protected $primaryKey = 'id';
    protected $fillable = ['id','name', 'email', 'password'];
    public $timestamps = false;
    const student_session_key = 'login_student_59ba36addc2b2f9401580f014c7f58ea4e30989d';

    public function getAuthPassword(){
        return $this->password;
    }

    public static function studentSessionID()
    {
        return Session::get(self::student_session_key);
    }

    public static function getLoggedStudent() 
    {
        $studen_data = self::where('id', self::studentSessionID())->first();
        if(method_exists($studen_data, 'toArray')){
            return $studen_data->toArray();
        }
        return 0;
    }
}
