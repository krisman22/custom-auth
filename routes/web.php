<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::controller(StudentController::class)->group(function () {
    Route::get('/login_view','LoginViewHandler')->name('login.view');
    Route::post('/login_action','LoginActionHandler')->name('login.action');
    Route::get('/login_status','LoginStatusHandler')->name('login.status');
    Route::get('/logout_student','LogoutHandler')->name('logout_student');
});

require __DIR__.'/auth.php';
